from django.core.management.base import BaseCommand
import pandas as pd

from datetime import datetime


class Args(object):
    INPUT = 'input'
    A_INPUT = '-input'

    @staticmethod
    def get_input(options):
        return options[Args.INPUT][0]


class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        parser.add_argument(Args.A_INPUT, nargs=1, type=str)

    def handle(self, *args, **options):
        input = Args.get_input(options)

        hour = pd.read_csv(input,
                         sep=',',
                         names=['datetime', 'open', 'high', 'low', 'close', 'vopen', 'vhigh', 'vlow', 'vclose'],
                         parse_dates=True,
                         index_col='datetime',
                         float_precision="high")
        hour = hour['close']

        # 一時間あたりの情報
        diff = hour.diff(periods=1)
        abs_mean = diff.abs().mean()
        std = diff.std()
        print('1時間あたりの値動き量（平均）:' + str(abs_mean))
        print('1時間あたりの値動き量（標準偏差）:' + str(std))
        backward = hour.shift(-1)

        ratio = backward / hour - 1
        r_abs_mean = ratio.abs().mean()
        r_std = ratio.std()
        print('1時間あたりの値動き量（比率・平均）:' + str(r_abs_mean*100) + '%')
        print('1時間あたりの値動き量（比率・標準偏差）:' + str(r_std*100) + '%')

        # 一日あたりの情報
        day = hour.resample('D').ohlc().interpolate()['close']
        diff = day.diff(periods=1)
        abs_mean = diff.abs().mean()
        std = diff.std()
        print('一日あたりの値動き量（平均）:' + str(abs_mean))
        print('一日あたりの値動き量（標準偏差）:' + str(std))
        backward = day.shift(-1)

        ratio = backward / day - 1
        r_abs_mean = ratio.abs().mean()
        r_std = ratio.std()
        print('一日あたりの値動き量（比率・平均）:' + str(r_abs_mean * 100) + '%')
        print('一日あたりの値動き量（比率・標準偏差）:' + str(r_std * 100) + '%')
        
        """
        ❯ python manage.py base_static -input createcsv/management/files/hour_resampled.csv
        1時間あたりの値動き量（平均）:1241.33442053
        1時間あたりの値動き量（標準偏差）:6300.14969006
        1時間あたりの値動き量（比率・平均）:0.486299774464%
        1時間あたりの値動き量（比率・標準偏差）:0.952586177392%
        一日あたりの値動き量（平均）:6352.64993567
        一日あたりの値動き量（標準偏差）:28503.6968642
        一日あたりの値動き量（比率・平均）:2.44579523103%
        一日あたりの値動き量（比率・標準偏差）:4.15658952147%
        """
