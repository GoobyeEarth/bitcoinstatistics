from django.core.management.base import BaseCommand
import pandas as pd
import matplotlib.pyplot as plt


class Args(object):
    INPUT = 'input'
    A_INPUT = '-input'
    OUTPUT = 'output'
    A_OUTPUT = '-output'

    @staticmethod
    def get_input(options):
        return options[Args.INPUT][0]

    @staticmethod
    def get_output(options):
        return options[Args.OUTPUT][0]


class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        parser.add_argument(Args.A_INPUT, nargs=1, type=str)
        parser.add_argument(Args.A_OUTPUT, nargs=1, type=str)

    def handle(self, *args, **options):
        input = Args.get_input(options)
        output = Args.get_output(options)

        hour = pd.read_csv(input,
                           sep=',',
                           names=['datetime', 'open', 'high', 'low', 'close', 'vopen', 'vhigh', 'vlow', 'vclose'],
                           parse_dates=True,
                           index_col='datetime',
                           float_precision="high")

        hour = hour['close']
        day = hour.resample('D').ohlc().interpolate()['close']

        day.plot(logy=True)
        plt.show()

