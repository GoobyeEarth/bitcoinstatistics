from django.core.management.base import BaseCommand
import pandas as pd

from datetime import datetime


class Args(object):
    BASE = 'base'
    A_BASE = '-base'
    OUTPUT = 'output'
    A_OUTPUT = '-output'

    @staticmethod
    def get_base(options):
        return options[Args.BASE][0]

    @staticmethod
    def get_output(options):
        return options[Args.OUTPUT][0]


class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        parser.add_argument(Args.A_BASE, nargs=1, type=str)
        parser.add_argument(Args.A_OUTPUT, nargs=1, type=str)

    def handle(self, *args, **options):
        base = Args.get_base(options)
        output = Args.get_output(options)

        def date_parser(unix_timestamp):
            return datetime.fromtimestamp(float(unix_timestamp))

        df = pd.read_csv(base,
                         sep=',',
                         names=['datetime', "price", 'volume'],
                         parse_dates=True,
                         date_parser=date_parser,
                         index_col='datetime',
                         float_precision="high")

        df = df.resample('H').ohlc().interpolate()
        df.to_csv(output, index=True, encoding='utf-8', mode='w', header=False)



