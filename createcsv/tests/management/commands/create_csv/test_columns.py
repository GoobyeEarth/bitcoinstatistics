from django.test import TestCase
from createcsv.management.commands.resample import Column


class ColumnTest(TestCase):
    def test_read_unix_time(self):
        row = ['1414735929', '36880.000000000000', '0.095100000000']
        actual = Column.read_unix_time(row)
        self.assertEqual(str(actual), '2014-10-31 06:12:09')
